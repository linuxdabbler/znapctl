PREFIX = /usr
MANDIR = $(PREFIX)/share/man
APTDIR = /etc/apt/apt.conf.d
CONFDIR = /etc/znapctl
SUDODIR = /etc/sudoers.d

all:
	@echo Run \'make install\' to install znapctl

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/local/bin
	@mkdir -p $(DESTDIR)$(MANDIR)/man1
	@mkdir -p $(DESTDIR)$(CONFDIR)
	@mkdir -p $(DESTDIR)$(SUDODIR)
	@cp -p znapctl $(DESTDIR)$(PREFIX)/local/bin/znapctl
	@cp -p znapctl.1 $(DESTDIR)$(MANDIR)/man1/znapctl.1
	@cp -p 80zfsnapshotconf $(DESTDIR)$(APTDIR)/80zfsnapshotconf
	@cp -p znapctl.conf $(DESTDIR)$(CONFDIR)/znapctl.conf
	@chmod 755 $(DESTDIR)$(PREFIX)/local/bin/znapctl
	@cp -p znapctl-sudoers $(DESTDIR)$(SUDODIR)/znapctl
	@chown root:root $(DESTDIR)$(SUDODIR)/znapctl

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/local/bin/znapctl
	@rm -rf $(DESTDIR)$(MANDIR)/man1/ns.1*
	@rm -rf $(DESTDIR)$(APTDIR)/zfsnapshotconf
	@rm -rf $(DESTDIR)$(CONFDIR)/znapctl.conf
	@rm -rf $(DESTDIR)$(SUDODIR)/znapctl

