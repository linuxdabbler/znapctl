<h1 style="text-align: center;">znapctl - zfs snapshot script</h1>

---

<h3 style="text-align: center;">simple command line tool for creating, deleting, listing, and rolling back zfs snapshots written in bash.</h3>

---

Znapctl is a command line tool written in bash for creating, listing, deleting, and rolling back zfs snapshots.  It uses dmenu for a selection menu to perform tasks.

There are 4 types of snapshots, root_snapshot, boot_snapshot, home_snapshot, and misc_snapshot along with a formatted date and time at the end.

For root, there are different lables that are applied to each snapshot depending on what triggered it.  

        * manual labels are automatically applied to snapshots when a second argument is not given when the script in invoked.

        * apt_automatic labels are applied to snapshots that were made when a package was installed/removed/modified on your system through an apt hook in 80znapshotconf.  this is located in /etc/apt/apt.conf.d/   

        * hourly_automatic through yearly_automatic labels are applied to snapshot that were made with a cron job.

The user is able to configure the number of allowed snapshots for each snapshot type in the znapctl.conf file located in /etc/znapctl/znapctl.conf or in ~/.config/znapctl/znapctl.conf
        
<img src="./images/znapctl-menu.png" align="center" height="360px">

---
<img src="./images/znapctl-menu-root.png" align="center" height="360px">

---

<img src="./images/znapctl-help.png" align="center" height="360px">

---

<img src="./images/znapctl-list.png" align="center" height="720px">

---

<img src="./images/znapctl-root.png" align="center" height="100px">

---

### DEPENDENCIES
This script depends on fzf, and zfs modlules being installed.
